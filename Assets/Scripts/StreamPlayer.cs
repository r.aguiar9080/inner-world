﻿using AudioSharp;
using AudioSharp.Codecs.WAV;
using AudioSharp.CoreAudioAPI;
using AudioSharp.SoundIn;
using AudioSharp.Streams;
using AudioSharp.Streams.SampleConverter;
using System;
using UnityEngine;

//[CLSCompliant(false)]
public class StreamPlayer : MonoBehaviour
{
    public int streamId = 0;
    public AudioSource source;
    public int networkLatency = 20;

    private float[] streamRingBuffer = new float[9990000];
    private byte[] tmpStreamBuffer = new byte[9990000];
    private int streamReadPos = 0;
    private int streamWritePos = 0;
    private WasapiCapture capture = new WasapiLoopbackCapture();
    private IWaveSource finalSource;
    private WaveWriter w;

    // Use this for initialization
    void Start()
    {
        try
        {
            for (int i = 0; i < streamRingBuffer.Length; i++)
            {
                //streamRingBuffer[i] = UnityEngine.Random.Range(-1.0f, 1.0f);
                streamRingBuffer[i] = 0.0f;
            }
            AudioClip myClip = AudioClip.Create("dummy", 1, 1, AudioSettings.outputSampleRate, false);
            //AudioClip myClip = AudioClip.Create("dummy", 1, 1, 44100, false);
            myClip.SetData(new float[] { 1 }, 0);
            source.clip = myClip;
            source.loop = true;
            source.spatialBlend = 0;
            source.Play();

            // This uses the wasapi api to get any sound data played by the computer
            //var deviceEnum = new MMDeviceEnumerator();
            //var devices = deviceEnum.EnumAudioEndpoints(DataFlow.All, DeviceState.Active);
            capture = new WasapiLoopbackCapture();
            //capture.Device = devices[1];

            //capture = new WasapiCapture();
            //capture.Device = devices[4];

            capture.Initialize();
            /*
            var soundInSource = new SoundInSource(capture)
            { FillWithZeros = true }; //set FillWithZeros to true, to prevent WasapiOut from stopping for the case WasapiCapture does not serve any data

            var soundOut = new WasapiOut();
            soundOut.Initialize(soundInSource);
            soundOut.Play();*/

            // Get our capture as a source
            //var sourceIWave = new SoundInSource(capture, 65536) { FillWithZeros = false };
            var sourceIWave = new SoundInSource(capture) { FillWithZeros = false };
            // Tells us when data is available to send to our spectrum
            //var notificationSource = new SingleBlockNotificationStream(sourceIWave.ToSampleSource());
            //finalSourcePCM = new SampleToPcm16(sourceIWave.ToSampleSource());
            finalSource = new SampleToIeeeFloat32(sourceIWave.ToSampleSource());
            //finalSourcePCM = sourceIWave.ToSampleSource().ToWaveSource();
            w = new WaveWriter("wave.wav", finalSource.WaveFormat);
            // We use this to request data so it actualy flows through (figuring this out took forever...)
            //finalSource = notificationSource.ToWaveSource();
            //finalSource = sourceIWave.ToSampleSource().ToWaveSource();
            sourceIWave.DataAvailable += Capture_DataAvailable;
            //notificationSource.SingleBlockRead += NotificationSource_SingleBlockRead;
            capture.Start();
        }
        catch (Exception e)
        {
            Debug.Log("Exception: " + e.ToString());
        }
    }/*
    // mark MessagePackObjectAttribute
    [MessagePackObject]
    public class MyClass
    {
        // Key is serialization index, it is important for versioning.
        [Key(0)]
        public int Age { get; set; }

        [Key(1)]
        public string FirstName { get; set; }

        [Key(2)]
        public string LastName { get; set; }

        // public members and does not serialize target, mark IgnoreMemberttribute
        [IgnoreMember]
        public string FullName { get { return FirstName + LastName; } }
    }*/

    private void Capture_DataAvailable(object sender, DataAvailableEventArgs e)
    {
        //DeviceFormat: "ChannelsAvailable: 2|SampleRate: 48000|Bps: 384000|BlockAlign: 8|BitsPerSample: 32|Encoding: Extensible|SubFormat: 00000001-0000-0010-8000-00aa00389b71|ChannelMask: SpeakerFrontLeft, SpeakerFrontRight"
        //WaveFormat = "ChannelsAvailable: 2|SampleRate: 48000|Bps: 192000|BlockAlign: 4|BitsPerSample: 16|Encoding: Extensible|SubFormat: 00000001-0000-0010-8000-00aa00389b71|ChannelMask: SpeakerFrontLeft, SpeakerFrontRight"
        //Debug.Log("Read: " + e.ByteCount + ";" + e.Offset);

        //finalSource.Read(e.Data, e.Offset, e.ByteCount);
        //w.Write(e.Data, e.Offset, e.ByteCount);

        //int read = finalSource.Read(tmpStreamBuffer, 0, e.ByteCount);
        int readPCM = finalSource.Read(tmpStreamBuffer, 0, e.ByteCount);
        while (readPCM > 0)
        {
            //Debug.Log("Read Final: " + readPCM);
            //w.Write(tmpStreamBuffer, 0, read);
            w.Write(tmpStreamBuffer, 0, readPCM);

            var tmp = ConvertByteBigToFloat(tmpStreamBuffer, readPCM);
            Array.Copy(tmp, 0, streamRingBuffer, streamWritePos, tmp.Length);
            /*var mc = new MyClass
            {
                Age = 99,
                FirstName = "hoge",
                LastName = "huga",
            };
            Debug.Log("Seriel" + MessagePackSerializer.Serialize(mc));*/
            streamWritePos += tmp.Length;
            //read = finalSource.Read(tmpStreamBuffer, 0, e.ByteCount);
            readPCM = finalSource.Read(tmpStreamBuffer, 0, e.ByteCount);
        }
        //Debug.Log("Finished Reading: " + readPCM);
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && networkLatency < 100) networkLatency += 5;
        if (Input.GetKeyDown(KeyCode.DownArrow) && networkLatency > 0) networkLatency -= 5;
        if (Input.GetKeyDown(KeyCode.R)) restartBufferLatency();
    }

    private void OnDestroy()
    {
        try
        {
            capture.Stop();
            w.Dispose();
            capture.Dispose();
        }
        catch (Exception e)
        {
            Debug.Log("Exception: " + e.ToString());
        }
    }

    void OnAudioFilterRead(float[] data, int channels)
    {
        int count = 0;
        int finalStreamReadPos = (streamReadPos + data.Length) % streamRingBuffer.Length;
        //Debug.Log("Size: :" + data.Length);
        if (data.Length + streamReadPos >= streamRingBuffer.Length)
        {
            for (int i = streamReadPos; i < streamRingBuffer.Length; i++)
            {
                data[count] *= streamRingBuffer[streamReadPos + count];
                streamRingBuffer[streamReadPos + count] = 0;
                //Debug.Log(Mathf.Sin(2 * Mathf.PI * frequency * position / 44100));
                count++;
            }
            // trying to avoid several % operations due to very time critical
            streamReadPos = 0;
        }
        while (count < data.Length)
        {
            data[count] *= streamRingBuffer[streamReadPos + count];
            streamRingBuffer[streamReadPos + count] = 0;
            count++;
        }
        streamReadPos = finalStreamReadPos;
        if (streamReadPos > streamWritePos) {
            //Debug.Log("Had to trick and go back");
            //streamReadPos = Math.Max(streamWritePos - 10000, 0);
        }
        /*while (count < Math.Min(data.Length, floatArr.Length))
        {
            data[count] *= floatArr[readPos + count];
            //Debug.Log(Mathf.Sin(2 * Mathf.PI * frequency * position / 44100));
            count++;
        }*/
    }

    private void restartBufferLatency()
    {
        streamReadPos = streamWritePos - LatencyToSamples(networkLatency);
        if (streamReadPos > 0) streamReadPos += streamRingBuffer.Length;
    }

    private int LatencyToSamples(int latency)
    {
        return (int) Mathf.Round(latency * AudioSettings.outputSampleRate / 1000);
    }

    private float[] ConvertByteToFloat(byte[] array, int offset, int length)
    {
        float[] floatArr = new float[length / 2];
        for (int i = 0; i < floatArr.Length; i++)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(array, i * 2, 2);
            }
            floatArr[i] = (float)(BitConverter.ToInt16(array, offset + i * 2) / 32767f);
        }
        return floatArr;
    }

    private float[] ConvertByteBigToFloat(byte[] bytes, int length)
    {
        float[] floats = new float[length / 4];
        for (int i = 0; i < length; i += 4)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes, i * 2, 2);
            }
            floats[i / 4] = BitConverter.ToSingle(bytes, i); // convert 4 bytes to float
        }
        return floats;

    }

    static float[] PCMBytesToFloats(byte[] bytes, int length)
    {
        float[] floats = new float[length / 2];
        for (int i = 0; i < length; i += 2)
        {
            floats[i / 2] = ((float)BitConverter.ToInt16(bytes, i)) / (float)32768f; // convert 2 bytes (short) to float
        }
        return floats;
    }
}